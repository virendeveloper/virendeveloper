<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    function is_host_registered($email)
    {
        
            $this->db->select('*'); 
            $this->db->from('tbl_users');
            $this->db->where('email_id', $email);
            $result = $this->db->get()->row_array();

            return $result;
        
    }
    
}