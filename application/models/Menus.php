<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menus extends CI_Model {
    function get_category_count($where2='')
    {
        if($this->input->post('search'))
        {
            $this->db->group_start();
            $this->db->or_like('category_name',$this->input->post('search'),'after');
            $this->db->group_end();
        }
        return $this->db->where('user_id',  $this->session->user_id)->count_all_results('tbl_category');
    }
    
    function get_category_all($limit,$start,$where2='')
    {
        $this->db->select('*');
        if($this->input->post('search'))
        {
            $this->db->group_start();
            $this->db->or_like('category_name',$this->input->post('search'),'after');
            $this->db->group_end();
        }
        $this->db->where('user_id',  $this->session->user_id)->order_by('_id','Desc');
        if($limit>0)
        {
            $this->db->limit($limit,$start);
        }
        $res = $this->db->get('tbl_category');
        if($res->num_rows()>0)
        {
            $data = [];
            foreach($res->result() as $category)
            {
                $category->parent = 'NA';
                if($category->parent_id>0)
                {
                    $event->parent = $this->db->select('category_name')->get_where('tbl_category',['_id'=>$category->parent_id])->row()->category_name;
                }
                $data[] = $category;
            }
            return ['status'=>TRUE,'data'=>$data];
        }else{
            return ['status'=>FALSE];
        }
    }
}