 <!--// Error Page Content -->

            <!-- Copyright -->
            <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center">
                <p>&COPY; 2018 Yoozik . All Rights Reserved </p>
            </div>
            <!--// Copyright -->
        </div>
    </div>


   
    <!-- //Required common Js -->

    <!-- Sidebar-nav Js -->
    <script>
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    <!--// Sidebar-nav Js -->

    <!-- dropdown nav -->
    <script>
        $(document).ready(function () {
            $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                    $(this).toggleClass('open');
                }
            );
        });
        function show_notify(msg,type)
        {
            $.notify({
                // options
                message: msg
                },{
                // settings
                type: type,
                delay: 5000,
                z_index: 99999
            });
        }
    </script>
    <!-- //dropdown nav -->

    <!-- Js for bootstrap working-->
    <script src="<?=BASE?>assets/users/js/bootstrap.min.js"></script>
     <script src="<?=BASE?>assets/users/js/bootstrap-notify.min.js"></script>
    <!-- //Js for bootstrap working -->

</body>

</html>