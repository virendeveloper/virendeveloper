

            <!-- main-heading -->
            <h2 class="main-title-w3layouts mb-2 text-center">Category</h2>
            <!--// main-heading -->

            <!-- Error Page Content -->
            <div class="blank-page-content">

                <!-- Error Page Info -->
                <div class="outer-w3-agile mt-3">
                    <h4 class="tittle-w3-agileits mb-4">Category List</h4>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Category Name</th>
                                    <th scope="col">Category Image</th>
                                    <th scope="col">Level</th>
                                    <th scope="col">Parent</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="category_body">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--// Error Page Info -->

            </div>
            <script>
                $(document).ready(function(){
                    fetch_category(0);
                    $('body').on('click','#paginate a',function(e){
                        if(!$(this).parent().hasClass('active'))
                        {
                            e.preventDefault();
                            var page = ($(this).attr("href")).split('/');
                            var page_number = page[page.length-1];
                            if(page_number)
                            fetch_category(page_number);
                            else{
                                fetch_category(0);
                            }
                        }
                        return false;
                    });
                })
    function fetch_category(id)
    {
        var page = parseInt(id)+1;
        $("#category_body").empty().append('<tr><td colspan="6" style="text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x"></i></td></tr>');
        var search = $('#search').val();
        if(id==''){ id=0;}
        $.ajax({
       type: "POST",
       url: '<?=BASE?>user/category/call-category/'+id,
       data:{ search:search},
       success: function(res){
           var data = JSON.parse(res);
           var j = parseInt(id);
           if(data.status)
           {
               var userdata = '';
               var holder = data.data;
               var length = holder.length;
               for(var i =0; i< length; i++)
               {
                   var perpage = 10;
                   var showing = perpage+page-1;
                   if((perpage)>data.query)
                   {
                       showing = perpage;
                   }
                   if(showing>data.query)
                   {
                       showing=data.query;
                   }
                   var img = '';
                   if(holder[i].category_image)
                   {
                       img = '<img src="<?=BASE?>assets/uploads/category/'+holder[i].category_image+'" style="width:100px;" />'
                   }
                  
                    userdata += '<tr>'+
                            '<td>'+ parseInt(j+1) +'</td>'+
                        '<td>'+holder[i].category_name+'</td>'+
                        '<td>'+img+'</td>'+
                        '<td>'+holder[i].level+'</td>'+
                        '<td>'+holder[i].parent+'</td>'+
                        '<td>'+
                           '<a class="btn btn-success btn-sm" title="edit" href="<?=BASE?>user/category/edit/'+holder[i]._id+'"><i class="fa fa-pencil"></i></a> '+
                           '<a class="btn btn-danger btn-sm in-del" title="Delete" href="javascript:void(0)" data-href="<?=BASE?>user/category/delete/'+holder[i].id+'"><i class="fa fa-trash"></i></a> ';
                        userdata +='</td>'+
                        '</tr>';
                    j++;
                }
                 var pagination='';
                if(data.links)
                {
                     pagination=data.links;
                }
                    userdata +='<tr><td colspan="3" style="padding: 30px 10px;">Showing '+page+' to '+showing+' of '+data.query+' entries</td><td id="paginate" class="text-right" colspan="3"> <ul class="pagination">'+pagination+'</ul></td></tr>';
          }else{
                userdata += '<tr><td colspan="6">No category found.</td></tr>'
          }
          $("#category_body").html(userdata);
       }
       });
    }
                </script>