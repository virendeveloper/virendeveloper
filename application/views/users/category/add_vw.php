<?php $page = $this->uri->segment(3);?>

            <!-- main-heading -->
            <h2 class="main-title-w3layouts mb-2 text-center">Category</h2>
            <!--// main-heading -->

            <!-- Error Page Content -->
            <div class="blank-page-content">

                <!-- Error Page Info -->
                    <div class="outer-w3-agile col-xl mt-3">
                        <div class="col-sm-6">
                            <h4 class="tittle-w3-agileits mb-4"><?=ucfirst($page)?> Category</h4>
                            <form action="#" method="post" id="catFrm" >
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Category name</label>
                                    <input type="text" class="form-control" value="<?=$category->category_name?>" name="category_name" id="category_name" placeholder="Category Name" >
                                    <?php if($page=='edit'){?>
                                    <input type="hidden" name="category_id" value="<?=$this->uri->segment(4)?>" />
                                    <?php }?>
                                    <div id="category_name_err"></div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Select Parent</label>
                                    <select class="form-control" name="parent" id="parent">
                                        <option value="">Select</option>
                                        <?php foreach($parents as $parent){?>
                                        <option <?=$category->parent_id==$parent->_id?'Selected':''?> value="<?=$parent->_id?>"><?=$parent->category_name?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect2">Category Image</label><br>
                                    <input type="file" name="image" />
                                    <?php if($category->category_image){?>
                                    <img src="<?=BASE?>assets/uploads/category/<?=$category->category_image?>" width="100px;" />
                                    <?php }?>
                                    <div id="image_err"></div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="catBtn" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                </div>
                <!--// Error Page Info -->

            </div>
            <script>
                $(document).ready(function(){
                     $('#catFrm').submit(function(e){
                          e.preventDefault();
                          $('#catBtn').attr('disabled','disabled').html('<i class="fa fa-spin fa-spinner"></i> Submit');
                            var url = '<?=BASE?>user/category/<?=$page?>-category';
                            var formData = new FormData(this);
                                $.ajax({
                                    type:'POST',
                                    url: url,
                                    data:formData,
                                    cache:false,
                                    contentType: false,
                                    processData: false,
                                    success:function(success){
                                var res = JSON.parse(success);
                                $('#catBtn').removeAttr('disabled').html('Submit');
                                if(res.status)
                                {
                                    <?php if($page=='add'){$msg = 'Added';}else{$msg = 'Updated';}?>
                                    show_notify('Category <?=$msg?>!','success');
                                    setTimeout(function(){window.location.href='<?=BASE?>user/category';},2000);
                                }else{
                                    var error = res.error;
                                    show_notify(res.msg,'danger');
                                    $.each( error, function( i, val ) 
                                    {
                                        $('#'+i).html(val);
                                    });
                                    }
                                }
                            })
                      })
                })
            </script>