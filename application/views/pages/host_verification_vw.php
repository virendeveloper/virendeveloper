<body class="body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8">
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png"  class="img-fluid"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png" class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start host verification -->
<section class="host-verification-page">
	<div class="container-fluid">
		<div class="row">
			<div class=" col-xl-8 col-lg-8 col-md-7 col-sm-8 col-8">
				<div class="checkin clearfix">
					<img src="<?=BASE?>assets/images/host-verification-checkin.png" class="img-fluid">
					<div class="checkin-left host-checkin">	
						<p>CHECK-INS</p>
						<h3>1</h3>
						<ul>
						  <li class="check-opacity"><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-4 offset-xl-2 col-lg-5 offset-lg-2 col-md-6 offset-md-2 col-sm-10 text-center">
				<img src="<?=BASE?>assets/images/host-verification.png" class="img-fluid">
				<div class="host-verification">
					<img src="<?=BASE?>assets/images/verification.png" class="img-fluid">
					<h5>Verification</h5>
					<p>Yoozik Account is free for Host & Diners Please verify your email and phone Number before using our app</p>
					<div class="checkin-left gray-check">
					  <ul>
						<li class="check-opacity"><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					  </ul>
					</div>	
					<div class="get-started-btn">
					  <a href="<?=BASE?>host-login-verify-phoneno" class="btn btn-yellow">Get Started</a>
					</div>
				</div>		  
			</div>
		</div>
	</div>
</section>
<!-- end host verification -->
</body>
</html>