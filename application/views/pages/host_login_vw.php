<body class="body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8">
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png" class="img-fluid"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png" class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start host login -->
<section class="host-login-page">
	<div class="container">
		<div class="host-login-top">
			<img src="<?=BASE?>assets/images/login-top.png" class="img-fluid">
		</div>
		<div class="row">
			<div class="col-xl-4 offset-xl-3 col-lg-4 offset-lg-3 col-md-5 offset-md-3 col-sm-6 offset-sm-3 col-9 offset-1">
				<div class="host-login-wrapper text-center">
					<img src="<?=BASE?>assets/images/chef.png" class="img-fluid">
					<p>Signup with Facebook Business Page</p>
					<!-- start login form -->
					<div class="button-login">
						<a href="<?=BASE?>host-login-social-networks" class="btn btn-gray">Login now</a>
						<a href="<?php echo $authURL; ?>" class="btn btn-blue"> <i class="fa fa-facebook-square" aria-hidden="true"></i>Continue with Facebook</a>
					</div>
					<span>Don't have an account? <a href="<?=BASE?>register-now"> Register now</a></span>
				</div>
			</div>	
		</div>
	</div>
</section>
</body>
</html>