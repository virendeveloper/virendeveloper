<style type="text/css">
	.error{
		color: red;
	}
</style>
<body class="body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8">
					<?php if($this->session->flashdata('error')) { ?>

				         <div class="alert alert-danger alert-dismissible" id="errorDiv">
				         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				         <?php  echo $this->session->flashdata('error'); ?>
				         </div> 

				      <?php } else if($this->session->flashdata('success')) { ?>

				         <div class="alert alert-success alert-dismissible" id="errorDiv">
				         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				         <?php  echo $this->session->flashdata('success'); ?>
				         </div>

				      <?php } ?>
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png"  class="img-fluid"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png" class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start register now detail -->
<section class="register-now-detail">
	
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-8">
				<div class="registernow-detail-wrapper text-center">
					<img src="<?=BASE?>assets/images/register-now.png" class="img-fluid">
				</div>
			</div>
			<div class="col-xl-5 col-lg-5">
				<div class="login-social-nw-wrapper">
					<div class="hostlogin-social-title">
						<a href="#"><img src="<?=BASE?>assets/images/left-arrow.png"></a>
						<h3>Register Now</h3>
						<p>Please login to continue using our app.</p>
					</div>
					<p class="hosting-account">Please note to Signup with Facebook either Google Bussiness Page This will Save us some process time creating you free Hosting account</p>
				</div>
				<div class="regi-now-detail" >
                                    
                       <form  class="regi-now-detail-form" action="<?php echo base_url('auth/signup'); ?>" method="post" enctype="multipart/form-data" >
					  <ul class="form-list row">
                        <input type="hidden" value="<?php echo $auth_type; ?>" required name="auth_type" class="form-control">
                        <input type="hidden" value="<?php echo $social_id; ?>" required name="social_id" class="form-control">


						<li class="col-lg-12 col-md-12">
							<label>*Business Name</label>
							<div class="input-box">
                                <input type="text" required value="<?php echo set_value('business_name'); ?>" name="business_name" class="form-control" placeholder="MacDonald's">
							</div>
							<?php echo form_error('business_name'); ?>
						</li>
						<li class="col-lg-12 col-md-12">
							<label>*Business Address</label>
							<div class="input-box">
                                <input required type="text" value="<?php echo set_value('business_address'); ?>" name="business_address" class="form-control" placeholder="Khreshhatik St., 19A, Kiev, Ukraine">
							</div>
						</li>
						<li class="col-lg-12 col-md-12">
							<label>*Business Email</label>
							<div class="input-box">
                                <input type="email" <?php if(!empty($userdata) && isset($userdata['email']) ){ ?> value="<?php echo $userdata['email']; ?>" <?php } else { ?> value="<?php echo set_value('business_email'); ?>" <?php } ?>  required name="business_email" class="form-control" placeholder="Khreshhatik19A@MacDonalds.com">
							</div>
							<?php echo form_error('business_email'); ?>
						</li>
						<li class="col-lg-12 col-md-12">
							<label>Currency</label>
							<div class="input-box">
                                <select class="form-control" name="currency">
                                    <option value="">Select</option>
                                    <?php foreach($currencies as $currency){?>
                                    <option value="<?=$currency->_id?>"><?=$currency->currency_name?></option>
                                    <?php }?>
                                </select>
							</div>
						</li>
                         <li class="col-lg-12 col-md-12">
							<label>Language</label>
							<div class="input-box">
                                <select class="form-control" name="language">
                                    <option value="">Select</option>
                                    <?php foreach($languages as $language){?>
                                    <option value="<?=$language->_id?>"><?=$language->language_name?></option>
                                    <?php }?>
                                </select>
							</div>
						</li>
						<li class="col-lg-6 col-md-6">
							<label>*First Name</label>
							<div class="input-box">
                                <input <?php if(!empty($userdata)){ ?> value="<?php echo $userdata['first_name']; ?>" <?php }else{ ?> value="<?php echo set_value('first_name'); ?>"  <?php } ?> required type="text" name="first_name" class="form-control" placeholder="James">
							</div>
							<?php echo form_error('first_name'); ?>
						</li>
						<li class="col-lg-6 col-md-6">
							<label>*Last Name</label>
							<div class="input-box">
                                <input <?php if(!empty($userdata)){ ?> value="<?php echo $userdata['last_name']; ?>" <?php } else { ?> value="<?php echo set_value('last_name'); ?>" <?php } ?> required type="text" name="last_name" class="form-control" placeholder="dean">
							</div>
							<?php echo form_error('last_name'); ?>
						</li>
						<li class="col-lg-12 col-md-12">
							<label>Country</label>
							<div class="input-box">
                                <select onselect="selectcountry(this.value)" id="country" class="form-control" name="country">
                                    <option value="">Select</option>
                                    <?php foreach($countries as $country){?>
                                    <option value="<?=$country->iso?>"><?=$country->name?></option>
                                    <?php }?>
                                </select>
							</div>
						</li>
						<li class="col-lg-3 col-md-3">
							<label>*Phone code</label>
							<div class="input-box">
                                <input readonly id="phone_code" type="text" name="owner_name" class="form-control" placeholder="James dean">
							</div>
						</li>
						<li class="col-lg-9 col-md-9 mobile-phone">
							<label>Mobile Phone</label>
							<div class="input-box">
                                 <input  value="<?php echo set_value('mobile_no'); ?>" type="number" name="mobile_no" class="form-control" placeholder="63 678 76 40">
							</div>
							<?php echo form_error('mobile_no'); ?>
						</li>
						<li class="col-lg-6 col-md-6">
							<label>*Password</label>
							<div class="input-box">
                                <input required id="password" type="password" name="password" class="form-control">
							</div>
							<?php echo form_error('password'); ?>
						</li>
						<li class="col-lg-6 col-md-6e">
							<label>*Confirm password</label>
							<div class="input-box">
                                <input required id="confirm_password" type="password" name="confirm_password" class="form-control">
							</div>
							<?php echo form_error('confirm_password'); ?>
						</li>
						<li class="col-lg-12 col-md-12">
							<div class="upload-id">
                               <input type="file" name="image">
					   		   <img src="<?=BASE?>assets/images/regi-detail.png" class="img-fluid">
							</div>
						</li>
						<li class="col-lg-12 col-md-12">
							<div class="login-account">
                               <button type="submit" id="signupBtn" class="btn btn-blue">Register my account</button>
							</div>
						</li>
					  </ul>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>

	

	$("#country").change(function() {
	  var country=this.value;
		
			$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>auth/get_country/"+country,
			data:'keyword='+country,
			success: function(data)
			{
				var country_data = JSON.parse(data);
				$('#phone_code').val('+'+country_data[0].phonecode);
				
			},
			error: function(data)
			{
				console.log('error');
			}
		});
	});


</script>

<!-- end register now detail -->
</body>
</html>
<script>
	
	var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
