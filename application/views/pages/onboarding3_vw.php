<body class="body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="lets-start">
						<a href="<?=BASE?>onboarding4">Skip</a>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start On boarding -->
<section class="onbording3">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9">
				<div class="checkin clearfix">
					<div class="checkin-left">
						<p>CHECK-INS</p>
						<h3>3</h3>
						<ul>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li class="check-opacity"><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
						</ul>
					</div>
					<div class="checkin-right">
						<img src="<?=BASE?>assets/images/checkin.png" class="centered-img">
					</div>
				</div>
				<div class="yozik-dish">
					<div class="row align-items-center">
						<div class="col-lg-4 col-md-4 offset-lg-1">
						  <div class="yozik-dish-left text-center">
							<h5>Dish photos</h5>
							<p>Yoozik free app helps your diners Join us it simply a power brain saver Upload you dish photos always anytime guests immediate satisfaction results</p>
						  </div>
						</div>
						<div class="col-lg-7 col-md-7">
							<div class="yozik-dish-left">
								<img src="<?=BASE?>assets/images/yoozik-dish.png">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end On boarding -->
<!-- start yoozik-business-btm -->
<section class="yoozik-business-btm">
	<div class="container">
		<div class="row">
		    <div class="col-lg-4 col-md-4 ">
			  <div class="experience-content text-center">
				<img src="<?=BASE?>assets/images/business-int.png">
				<h4>Business intelligence</h4>
				<p>#goYoozik bussiness wise data analytics Yoozik inspries going Online menus! it's a free app for your diners Convenience viewing your DISH PHOTOS understanding online free translated DISH DESCRIPTION as well CURRENCY daily rates convertions.</p>
			  </div>
			</div>
			<div class="col-lg-4 col-md-4">
			  <div class="experience-content text-center">
				<img src="<?=BASE?>assets/images/cloud-comp.png">
				<h4>Cloud Computing</h4>
				<p>#goYoozik inspires going Online menus! it's a free app for Diners Saas cloud computing arcitucture Host side vsGuest side no hardware is requires! Simply internet connectivity</p>
			  </div>
			</div>
			<div class="col-lg-4 col-md-4">
			  <div class="experience-content text-center">
			    <img src="<?=BASE?>assets/images/global.png">
				<h4>Global</h4>
				<p>#goYoozik is a free app at your diners palm using simply there own mobiles so both hosts and diners diolog going online viewing Dish photos within traslated Dish description and Currencies convertions.</p>
			  </div>
			</div>
		</div>
	</div>
</section>
<!-- end yoozik-business-btm -->