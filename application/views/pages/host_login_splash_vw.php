<body class="hostlog-splsh body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-xl-8 col-lg-7 col-md-7 col-sm-7 col-6">
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png" class="img-fluid"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png" class="img-fluid"></a>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-5 col-md-5 col-sm-5 col-4">
					<div class="lets-start">
						<a href="<?=BASE?>onboarding">Let's Start</a>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start host login splash -->
<section class="host-login-splash">
	<div class="container">
		<div class="row">
			<div class="col-xl-10 col-lg-10 col-md-10 col-sm-12">
				<div class="row align-items-center">
					<div class="col-xl-4 col-lg-8 col-md-8 col-sm-10">
						<div class="host-login-revolution">
							<h3><img src="<?=BASE?>assets/images/revolution.png" class="img-fluid">Simple,yet revolutionary</h3>
							<p> <img src="<?=BASE?>assets/images/cloud_doc.png" class="img-fluid"> Saas cloud computing smart arciture for Hosts & Diners dialog</p>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
						<img src="<?=BASE?>assets/images/host-login-revolution.png" class="img-fluid">
					</div>
				</div>
				<div class="host-guest">
					<div class="row align-items-center">
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
							<div class="host-guest-wrap">
								<a href="#">
								  <img src="<?=BASE?>assets/images/chef-host.png" class="img-fluid">
								  <h4><img src="<?=BASE?>assets/images/host-login-checkbox.png" class="img-fluid">Host side</h4>
								</a>
							</div>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
							<div class="host-guest-logo">
								<img src="<?=BASE?>assets/images/host-guest-logo.png" class="img-fluid">
							</div>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
							<div class="host-guest-wrap">
								<a href="#">
								  <img src="<?=BASE?>assets/images/girl-host.png" class="img-fluid">
								  <h4><img src="<?=BASE?>assets/images/host-login-checkbox.png">Host side</h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end host login splash -->
<!-- start yoozik-business-btm -->
<section class="yoozik-business-btm">
	<div class="container">
		<div class="row">
		    <div class="col-xl-4 col-lg-4 col-md-4">
			  <div class="experience-content text-center">
				<img src="<?=BASE?>assets/images/business-int.png" class="img-fluid">
				<h4>Business intelligence</h4>
				<p>#goYoozik bussiness wise data analytics Yoozik inspries going Online menus! it's a free app for your diners Convenience viewing your DISH PHOTOS understanding online free translated DISH DESCRIPTION as well CURRENCY daily rates convertions.</p>
			  </div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4">
			  <div class="experience-content text-center">
				<img src="<?=BASE?>assets/images/cloud-comp.png" class="img-fluid">
				<h4>Cloud Computing</h4>
				<p>#goYoozik inspires going Online menus! it's a free app for Diners Saas cloud computing arcitucture Host side vsGuest side no hardware is requires! Simply internet connectivity</p>
			  </div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4">
			  <div class="experience-content text-center" class="img-fluid">
			    <img src="<?=BASE?>assets/images/global.png">
				<h4>Global</h4>
				<p>#goYoozik is a free app at your diners palm using simply there own mobiles so both hosts and diners diolog going online viewing Dish photos within traslated Dish description and Currencies convertions.</p>
			  </div>
			</div>
		</div>
	</div>
</section>