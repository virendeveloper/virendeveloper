<body class="body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8">
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png"  class="img-fluid"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png" class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start register now -->
<section class="register-now">
	<div class="container">
		<div class="row">
			<div class="col-xl-5 offset-xl-2 col-lg-6 offset-lg-2 col-md-6 offset-md-2 col-sm-6 offset-sm-2 col-10 offset-1">
				<div class="login-social-nw-wrapper">
					<div class="hostlogin-social-title">
						<a href="#"><img src="<?=BASE?>assets/images/left-arrow.png"></a>
						<h3>Register Now</h3>
						<p>Please login to continue using our app.</p>
					</div>
					<p class="hosting-account">Please note to Signup with Facebook either Google Bussiness Page <br>This will Save us some process time creating you free Hosting account</p>
					<div class="registernow-wrapper text-center">
						<img src="<?=BASE?>assets/images/register-now.png" class="centered-img">
						<ul class="register-now-button">
							<li><a href="<?php echo $authURL; ?>" class="btn btn-gray"><img src="<?=BASE?>assets/images/facebook.png">Continue with Facebook</a></li>
							<li><a href="#" class="btn btn-gray"><img src="<?=BASE?>assets/images/google.png">Continue with Google</a></li>
						</ul>
						<div class="regi-getstarted">
							<a href="<?=BASE?>host-register" class="btn btn-white">Get Started</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end register now detail -->
</body>
</html>