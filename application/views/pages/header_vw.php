<!DOCTYPE html>
<html>
<head>
	<title>Yoozik</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=BASE?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=BASE?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?=BASE?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?=BASE?>assets/css/responsive.css">

	<script type="text/javascript" src="<?=BASE?>assets/js/jquery.js"></script>	
	<script type="text/javascript" src="<?=BASE?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=BASE?>assets/js/function.js"></script>
        <script src="<?=BASE?>assets/users/js/bootstrap-notify.min.js"></script>
        <script>function show_notify(msg,type)
        {
            $.notify({
                // options
                message: msg
                },{
                // settings
                type: type,
                delay: 5000,
                z_index: 99999
            });
        }</script>
</head>