<body class="body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8">
					<?php if($this->session->flashdata('error')) { ?>

				         <div class="alert alert-danger alert-dismissible" id="errorDiv">
				         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				         <?php  echo $this->session->flashdata('error'); ?>
				         </div> 

				      <?php } else if($this->session->flashdata('success')) { ?>

				         <div class="alert alert-success alert-dismissible" id="errorDiv">
				         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				         <?php  echo $this->session->flashdata('success'); ?>
				         </div>

				      <?php } ?>
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png" class="img-fluid"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png" class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start host login social network -->
<section class="host-login-social-nw">
	<div class="container">
		<div class="row">
			<div class="col-xl-5 offset-xl-2 col-lg-6 offset-lg-2 col-md-6 offset-md-2 col-sm-6 offset-sm-2 col-10 offset-1">
				<div class="login-social-nw-wrapper">
					<div class="hostlogin-social-title">
						<a href="<?=BASE?>host-login"><img src="<?=BASE?>assets/images/left-arrow.png"  class="img-fluid"></a>
						<h3>Login Now</h3>
						<p>Please login to continue using our Yoozik Host free account</p>
					</div>
					<p class="hosting-account">Please note to Signup with Facebook either Google Bussiness Page This will Save us some process time creating you free Hosting account</p>
					<div class="hostlogin-social-wrapper">
						<p>Login instantly:</p>
						<div class="login-instant-button row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<a href="<?php echo $authURL; ?>" class="btn btn-border"><img src="<?=BASE?>assets/images/facebook.png"></a>
							</div>
							<div class="col-lg-6 col-md-6  col-sm-6">
								<a href="#" class="btn btn-border"><img src="<?=BASE?>assets/images/google.png"></a>
							</div>
						</div>
						<p>or login with email/mobile</p>
                             <form  class="login-host-form" action="<?php echo base_url('auth/login'); ?>" method="post" enctype="multipart/form-data" >
							<ul class="form-list row">
							  <li class="col-lg-12 col-md-12">
								<label>Email ID or Mobile Number</label>
								<div class="input-box">
                                    <input type="email" name="email" class="form-control" placeholder="johndoe@gmail.com">
                                    <?php echo form_error('email'); ?>
								</div>
							  </li>
							  <li class="col-lg-12 col-md-12">
								<label>Password</label>
								<div class="input-box password">
									<a href="javascript:void(0)" onclick="show_hide_password()"" ><img src="<?=BASE?>assets/images/password.png"></a>
                                    <input type="Password" id="password" name="password" class="form-control" placeholder=".....">
                                    <?php echo form_error('password'); ?>
								</div>
							  </li>
							  <li class="col-lg-6 col-md-6">
							  	<div class="input-box remember">
						        	<input type="checkbox" id="tc" name="agree" value="1" checked="">
						        	<label for="tc">Remember me</label>
						    	</div>
						    	</li>
						    	<li class="col-lg-6 col-md-6">
						    	<div class="forgot text-right">
						    		<a href="#">Forgot Password?</a>
						    	</div>
							  </li>
							  <li class="col-lg-12 col-md-12">
							  	<div class="login-account">
                                    <button type="submit" id="signinBtn" class="btn btn-blue">Login to my account</button>
								</div>
							  </li>
							  <li class="col-lg-12 col-md-12 acc-regi">
							  	<p>Don't have an account? <a href="<?=BASE?>register-now">Register now</a></p>
							  </li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	function show_hide_password(){
	var x = document.getElementById("password");
	  if (x.type === "password") {
	    x.type = "text";
	  } else {
	    x.type = "password";
	  }
	}
</script>

<!-- end host login social network -->
</body>
</html>