<body class="body-yoozik">
<!-- Start header -->
<header>
	<div class="container-fluid">
		<div class="yoozik-header">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8">
					<div class="menu-logo">
						<a href="javascript:void(0)" id="toggle-menu"><img src="<?=BASE?>assets/images/toggle-open.png"  class="img-fluid"></a>
						<div class="logo">
							<a href="#"><img src="<?=BASE?>assets/images/logo.png"  class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- end header -->
<!-- start host login verify phone number -->
<section class="hlogin-verify-phnumber">
	<div class="container-fluid">
		<div class="row">
			<div class=" col-xl-8 col-lg-8 col-md-7 col-sm-8 col-8">
				<div class="checkin clearfix">
					<img src="<?=BASE?>assets/images/hostlogin-verify.png" class="img-fluid">
					<div class="checkin-left host-checkin">	
						<p>CHECK-INS</p>
						<h3>2</h3>
						<ul>
						  <li><a href="#"></a></li>
						  <li class="check-opacity"><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						  <li><a href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-8">
				<div class="verify-phnumber">
					<img src="<?=BASE?>assets/images/flygirl.png" class="centered-img">
					<form class="login-host-veryfyno">
					  <ul class="form-list row">
						<li class="col-lg-12 col-md-12">
							<label>Country Code</label>
							<div class="input-box">
								<img src="<?=BASE?>assets/images/yellow-tick.png">
								<input type="text" class="form-control" placeholder="+972       |">
							</div>
						</li>
						<li class="col-lg-12 col-md-12">
							<label>Mobile Number</label>
							<div class="input-box">
								<img src="<?=BASE?>assets/images/yellow-tick.png">
								<input type="text" class="form-control" placeholder="+972       |   054-7591554">
							</div>
						</li>
					  </ul>
					</form>
				</div>	  
			</div>
			<div class="col-xl-5 col-lg-4 col-md-4">
				<div class="phone-verify-img">
					<img src="<?=BASE?>assets/images/mobile-screen.png" class="img-fluid">
				</div>
			</div>
			<div class="col-xl-3 offset-xl-4 col-lg-4 offset-lg-3 col-md-4 offset-md-3">
				<div class="verify-phone-num text-center">
					<img src="<?=BASE?>assets/images/yellow-mobile.png">
					<p>please verify your phone number</p>
					<div class="get-started-btn">
					  <a href="<?=BASE?>host-login-verify-email-address" class="btn btn-yellow">Next Step</a>
					</div>
					<div class="checkin-left gray-check">
					  <ul>
						<li class="check-opacity"><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					  </ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end host login verify phone number -->

</body>
</html>