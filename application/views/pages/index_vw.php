
<body>
<!-- Start header -->
<header>
  <div class="container-fluid">
	<div class="main-header">
	  <div class="row">
		<div class="col-lg-7 col-md-7">
		  <!-- start logo -->
		  <div class="logo">
			<a href="#"><img src="<?=BASE?>assets/images/logo.png"></a>
		  </div>
		  <!-- end logo -->
		  <!-- start menu -->
		  <div class="menu">
		  	<ul>
		  		<li><a href="#"><img src="<?=BASE?>assets/images/home.png">Host</a></li>
		  		<li><a href="#"><img src="<?=BASE?>assets/images/guest.png">Guest free app</a></li>
		  		<li class="menu-margin"><a href="#dish-photo">Features</a></li>
		  		<li class="menu-margin"><a href="#pricing">Pricing</a></li>
		  	</ul>
		  </div>
		  <!-- end menu -->
		</div>
		<div class="col-lg-5 col-md-5">
			<div class="header-btn">
				<a href="<?=BASE?>host-login" class="login">Login</a>
				<a href="<?=BASE?>host-login-splash" class="btn btn-white">Get Started</a>
			</div>		
		</div>
	  </div>
	</div>
  </div>
</header>
<!-- end header -->
<!-- start banner -->
<div class="banner">
	<div class="banner-caption">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="banner-heading">
						<h3>Online free menus</h3>
						<h6><i class="fa fa-heart" aria-hidden="true"></i>Dismissing paper menus is our mission!</h6>
					</div>
					<div class="banner-content">
						<p><span>YOOZIK is free app dialog Hosts & Dinners </span> which are using there own mobiles 'EYES ARE EATING' which is simply a power brain saver</p>
						<ul class="banner-yoozik">
							<li><img src="<?=BASE?>assets/images/banner-yoozik1.png">DISH PHOTOS</li>
							<li><img src="<?=BASE?>assets/images/banner-yoozik2.png">TRANSLATED DISH DESCRIPTION</li>
							<li><img src="<?=BASE?>assets/images/banner-yoozik3.png">DAILY RATES CURRENCY CONVERSTIONS</li>
						</ul>
						<div class="get-started-btn">
							<img src="<?=BASE?>assets/images/cloud_doc.png">
							<a href="<?=BASE?>host-login-splash" class="btn btn-yellow">Get Started</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end banner -->
<!-- start user-experience -->
<section class="user-experience">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-12 offset-lg-2">
				<div class="user-experience-title text-center">
					<img src="<?=BASE?>assets/images/chef.png">
					<div class="section-title">
						<h2>User Experience,redefined</h2>
					</div>
					<p>onboarding online with us(#goYoozik)</p>
					<img src="<?=BASE?>assets/images/plane.png">
				</div>
			</div>
		</div>
		<div class="experience-wrapper">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="experience-content text-center">
						<img src="<?=BASE?>assets/images/business-int.png">
						<h4>Business intelligence</h4>
						<p>#goYoozik bussiness wise data analytics Yoozik inspries going Online menus! it's a free app for your diners Convenience viewing your DISH PHOTOS understanding online free translated DISH DESCRIPTION as well CURRENCY daily rates convertions.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="experience-content text-center">
						<img src="<?=BASE?>assets/images/cloud-comp.png">
						<h4>Cloud Computing</h4>
						<p>#goYoozik inspires going Online menus! it's a free app for Diners Saas cloud computing arcitucture Host side vsGuest side no hardware is requires! Simply internet connectivity</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="experience-content text-center">
						<img src="<?=BASE?>assets/images/global.png">
						<h4>Global</h4>
						<p>#goYoozik is a free app at your diners palm using simply there own mobiles so both hosts and diners diolog going online viewing Dish photos within traslated Dish description and Currencies convertions.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end user-experience -->
<!-- start revolutionary -->
<section class="revolutionary">
	<div class="container">
		<div class="revolutionary-title text-center">
			<h3><img src="<?=BASE?>assets/images/revolution.png">Simple,yet revolutionary</h3>
			<p> <img src="<?=BASE?>assets/images/cloud_doc.png"> Saas cloud computing smart arciture for Hosts & Diners dialog</p>
		</div>
		<div class="revolutionary-content text-center">
			<img src="<?=BASE?>assets/images/revolutionary.png">
			<div class="get-started-btn text-center">
				<img src="<?=BASE?>assets/images/cloud_doc.png" class="centered-img">
				<a href="<?=BASE?>host-login-splash" class="btn btn-yellow">Get Started</a>
			</div>
		</div>
	</div>
</section>
<!-- end revolutionary -->
<!-- start dish photos -->
<section class="dish-photo">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-4">
				<div class="dish-wrapper text-center">
					<img src="<?=BASE?>assets/images/dish1.png">
					<h5>Dish photos</h5>
					<span>'Eyes are Eating'</span>
					<p>Yoozik free app helps your diners Join us it simply a power brain saver Upload you dish photos always anytime guests immediate satisfaction results</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="dish-wrapper text-center">
					<img src="<?=BASE?>assets/images/dish-globe.png">
					<h5>Dish Translations</h5>
					<span>'Eyes are Eating'</span>
					<p>Yoozik free app helps your diners Join us it simply a power brain saver Upload you dish photos always anytime guests immediate satisfaction results</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="dish-wrapper text-center">
					<img src="<?=BASE?>assets/images/dish-currency.png">
					<h5>Currencies Convertions</h5>
					<span>'Eyes are Eating'</span>
					<p>Yoozik free app helps your diners Join us it simply a power brain saver Upload you dish photos always anytime guests immediate satisfaction results</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end dish photos -->
<!-- start pricing -->
<section class="pricing">
  <div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-12 offset-lg-2 text-center">
			<img src="<?=BASE?>assets/images/logo.png">
			<div class="pricing-title">
				<h3>Affordable Pricing</h3>
				<p>Extended featurs explanation in our Pricing page</p>
			</div>
		</div>
	</div>	
	<div class="billed-wrapper">
	  <form class="submit-rating">
		<input id="quartly"  name="satisfaction" type="radio" /> 
		<input id="monthly" name="satisfaction" type="radio" /> 
		<label for="quartly" class="rating-label rating-label-meh">Billed Quartly</label>
		<div class="smile-rating-toggle"></div>
		<div class="toggle-rating-pill"></div>
		<label for="monthly" class="rating-label rating-label-fun">Billed Monthly</label>
	  </form>
	</div>
	<!-- start pricing table -->
	<div class="pricing-table">
		<div class="row align-items-center">
			<div class="col-lg-3 col-md-3 offset-lg-1">
				<div class="pricing-wrapper">
					<h4>Entry level</h4>
					<ul class="pricing-item">
						<li><img src="<?=BASE?>assets/images/check-arrow.png">1 menu</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Dish Photos</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Dish Traslation</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Dish Currencies Convertions</li>
					</ul>
					<p><sup>$</sup>00.00<sub>/mo</sub></p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 offset-lg-1">
				<div class="pricing-wrapper">
					<h4>Amateur level</h4>
					<ul class="pricing-item">
						<li><img src="<?=BASE?>assets/images/check-arrow.png">1-5 menus</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Basic Business intelligence</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">menus Controls</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Basic features</li>
					</ul>
					<p><sup>$</sup>119.99<sub>/mo</sub></p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 offset-lg-1">
				<div class="pricing-wrapper">
					<h4>Professional level</h4>
					<ul class="pricing-item">
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Unlimited menu</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Pro Business Intelegent</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Professional Services</li>
						<li><img src="<?=BASE?>assets/images/check-arrow.png">Pro features</li>
					</ul>
					<p><sup>$</sup>499.99<sub>/mo</sub></p>
				</div>
			</div>
		</div>
	</div>
	<div class="get-started-btn text-center">
		<a href="<?=BASE?>host-login-splash" class="btn btn-yellow">Get Started</a>
	</div>
  </div>	
</section>
<!-- end pricing -->
<footer class="mainlanding-footer">
	<div class="container">
		<img src="<?=BASE?>assets/images/chef.png" class="centered-img">
		<div class="footer-content">
			<div class="row">
				<div class="col-lg-2 offset-lg-1 col-md-2">
					<h3 class="footer-title">Product</h3>
					<ul>
						<li><a href="#">features</a></li>
						<li><a href="#">use cases</a> </li>
						<li><a href="#">Pricing</a></li>
					</ul>
				</div>
				<div class="col-lg-2 col-md-2">
					<h3 class="footer-title">use cases</h3>
					<ul>
						<li><a href="#">ites</a></li>
						<li><a href="#">Consultancies</a> </li>
						<li><a href="#">services sector</a></li>
					</ul>
				</div>
				<div class="col-lg-2 col-md-2">
					<h3 class="footer-title">support</h3>
					<ul>
						<li><a href="#">blog</a></li>
						<li><a href="#">FAQ'S</a> </li>
						<li><a href="#">Support</a></li>
					</ul>
				</div>
				<div class="col-lg-2 col-md-2">
					<h3 class="footer-title">Company</h3>
					<ul>
						<li><a href="#">About</a></li>
						<li><a href="#">Privacy Policy</a> </li>
						<li><a href="#">Terms of Service</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
</body>
</html>