<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller {
    private $userd;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('users');
        $this->load->library('facebook');
		$this->load->model('auth_model');
        $this->userd = $this->users->get_user($this->session->user_id,'_id,first_name,email_id,mobile_no');
    }

    function _is_logged()
    {
        if($this->session->user_id)
        {
            redirect('home');
        }
    }
            
    function index()
    {
        $pagedata['title']='Home';
        $this->load->view('pages/header_vw');
        $this->load->view('pages/index_vw');
        $this->load->view('pages/footer_vw');
    }
    
    function host_login_splash()
    {
        $pagedata['title']='Home';
        $this->load->view('pages/header_vw');
        $this->load->view('pages/host_login_splash_vw');
        $this->load->view('pages/footer_vw');
    }
    
    function onboarding()
    {
        $pagedata['title']='Home';
        $this->load->view('pages/header_vw');
        $this->load->view('pages/onboarding_vw');
        $this->load->view('pages/footer_vw');
    }
    function onboarding2()
    {
        $pagedata['title']='Home';
        $this->load->view('pages/header_vw');
        $this->load->view('pages/onboarding2_vw');
        $this->load->view('pages/footer_vw');
    }
    
    function onboarding3()
    {
        $pagedata['title']='Home';
        $this->load->view('pages/header_vw');
        $this->load->view('pages/onboarding3_vw');
        $this->load->view('pages/footer_vw');
    }
    
    function onboarding4()
    {
        $pagedata['title']='Home';
        $this->load->view('pages/header_vw');
        $this->load->view('pages/onboarding4_vw');
        $this->load->view('pages/footer_vw');
    }
    
    function onboarding5()
    {
        $pagedata['title']='Home';
        $this->load->view('pages/header_vw');
        $this->load->view('pages/onboarding5_vw');
        $this->load->view('pages/footer_vw');
    }
    
    function host_login()
    {
        $pagedata['title']='About';
		$pagedata['authURL'] =  $this->facebook->login_url();
        $this->load->view('pages/header_vw',$pagedata);
        $this->load->view('pages/host_login_vw');
    }
    
    function host_login_social_networks()
    {
        $pagedata['title']='Contact';
        $pagedata['authURL'] =  $this->facebook->login_url();
        $this->load->view('pages/header_vw',$pagedata);
        $this->load->view('pages/host_login_social_networks');
    }
    
    
    
    function host_verification()
    {
        $pagedata = ['userd'=>$this->userd];
        $pagedata['title']='Training';
        $this->load->view('pages/header_vw',$pagedata);
        $this->load->view('pages/host_verification_vw');
    }
    function host_login_verify_phoneno()
    {
        $pagedata = ['userd'=>$this->userd];
        $pagedata['title']='Training';
        $this->load->view('pages/header_vw',$pagedata);
        $this->load->view('pages/host_login_verify_phoneno_vw');
    }
    function host_login_verify_email_address()
    {
        $pagedata = ['userd'=>$this->userd];
        $pagedata['title']='Training';
        $this->load->view('pages/header_vw',$pagedata);
        $this->load->view('pages/host_login_verify_email_address_vw');
    }
    
    function register_now()
    {
        $pagedata = ['userd'=>$this->userd];
        $pagedata['title']='Training';
		$userData = array();
		$pagedata['authURL'] =  $this->facebook->login_url();
        $this->load->view('pages/header_vw',$pagedata);
        $this->load->view('pages/register_now_vw');
    }
	
	
	function social_response_for_facebook()
    {
        
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['first_name']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['link']        = !empty($fbUser['link'])?$fbUser['link']:'';
            
            $result = $this->auth_model->is_host_registered($userData['email']);

			if(!$result || empty($result)){
				// $this->session->set_userdata('socialData', $userData);
				$this->host_register($userData);
            }else{
               echo "successfully login";die;
            }
            
            // Get logout 
            $pagedata['logoutURL'] = $this->facebook->logout_url();
        }else{
            // Get login URL
            $pagedata['authURL'] =  $this->facebook->login_url();
        }
		
        
    }
    
    function host_register($userData = null)
    {
	        
        $pagedata = ['userd'=>$this->userd];
        $pagedata['currencies'] = $this->db->get_where('tbl_currency',['status'=>1])->result();
        $pagedata['languages'] = $this->db->get_where('tbl_language',['status'=>1])->result();
        $pagedata['countries'] = $this->db->get_where('tbl_country')->result();
        $pagedata['title']='Training';

        
        if($userData != null){

            $pagedata['userdata'] =  $userData;
            $pagedata['auth_type'] =  1;
            $pagedata['social_id'] =  $userData['oauth_uid'];

        }else{
            $pagedata['userdata'] =  "";
            $pagedata['auth_type'] =  0;
            $pagedata['social_id'] =  "";
        }
        
        $this->load->view('pages/header_vw',$pagedata);
        $this->load->view('pages/host_register_vw');
    }

    function get_country($country_id)
    {
    
        
        $country_data = $this->db->get_where('tbl_country',['iso'=>$country_id])->result();
        echo json_encode($country_data);
       
    }

    

    function games()
    {
        $pagedata = ['userd'=>$this->userd];
        $pagedata['title']='Training';
        $this->load->view('common/header_vw',$pagedata);
        $this->load->view('pages/games_vw');
        $this->load->view('common/footer_vw');
    }
    function initiatives()
    {
        $pagedata = ['userd'=>$this->userd];
        $pagedata['title']='Training';
        $this->load->view('common/header_vw',$pagedata);
        $this->load->view('pages/initiatives_vw');
        $this->load->view('common/footer_vw');
    }
    function faq()
    {
        $pagedata['title']='FAQ';
        $this->load->view('common/header_vw',$pagedata);
        $this->load->view('pages/faq_vw');
        $this->load->view('common/footer_vw');
    }
            
    function login()
    {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run())
        {
            $username=  $this->input->post('email');
            $password= md5(HASH.$this->input->post('password'));

            $log_check=$this->db->select('_id,verified')->get_where('tbl_users',['email_id'=>$username,'password'=>$password]);
            $data = $log_check->result_array();
            if(empty($data)){
                $this->session->set_flashdata('error', "Invalid login credentials.");
                $pagedata['title']='Contact';
                $this->load->view('pages/header_vw',$pagedata);
                $this->load->view('pages/host_login_social_networks');
            }else{
                print_r("Successfully login");die;
            }
        }else{
            $pagedata['title']='Contact';
            $this->load->view('pages/header_vw',$pagedata);
            $this->load->view('pages/host_login_social_networks');
        }
    }
    
    function signup()
    {
	

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
        $this->form_validation->set_rules('first_name','First name','trim|required|xss_clean|alpha_numeric_spaces');
        $this->form_validation->set_rules('last_name','Last name','trim|required|xss_clean|alpha_numeric_spaces');
        $this->form_validation->set_rules('business_email','Email','trim|required|xss_clean|valid_email|is_unique[tbl_users.email_id]');
        $this->form_validation->set_rules('password','Password','required|matches[confirm_password]|min_length[6]|max_length[20]');
        $this->form_validation->set_rules('confirm_password','Confirm password','required');
        $this->form_validation->set_rules('mobile_no','Mobile No','trim|xss_clean|min_length[8]|max_length[15]|is_unique[tbl_users.mobile_no]');
        $this->form_validation->set_rules('business_name','Business name','trim|required|xss_clean');
        $this->form_validation->set_rules('business_address','Business address','trim|required|xss_clean');

        $this->form_validation->set_message('is_unique', 'The %s already taken, Try another.');
        $this->form_validation->set_message('min_length', 'The %s is invalid, Try another.');
        $this->form_validation->set_message('max_length', 'The %s is invalid, Try another.');
        if($this->form_validation->run())
        { 
		
			/* $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['first_name']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['link']        = !empty($fbUser['link'])?$fbUser['link']:''; */
			$code=rand(0000,99999);
			$user_array = array(
        					'first_name'=>$this->input->post('first_name'),
							'last_name'=>$this->input->post('last_name'),
                            'business_name'=>$this->input->post('business_name'),
                            'business_address'=>$this->input->post('business_address'),
                            'email_id'=>$this->input->post('business_email'),
                            'currency'=>$this->input->post('currency'),
                            'currency'=>$this->input->post('currency'),
        					'country_id'=>$this->input->post('country'),
        					'language'=> $this->input->post('language'),
                            'mobile_no' => $this->input->post('mobile_no'),
                            'password' => md5(HASH.$this->input->post('password')),
							'user_type' => "host",
							'status'=>1,
							'verification_code'=>$code,
							'created_at'=>time(),
        				);

            if($this->input->post('auth_type') == 1){
                $user_array['login_type'] = 1;
                $user_array['social_id'] = $this->input->post('social_id');

            }else if($this->input->post('auth_type') == 2){
                $user_array['login_type'] = 1;
            }else {
                $user_array['login_type'] = 1;
            }


		
            
            $emailid = md5($this->input->post('email'));
            if($this->db->insert('tbl_users', $user_array))
            {


                $this->session->set_flashdata('success', "Congratulation! You are successfully registered as a host.");
				$this->host_login_social_networks();
                /* $email=array('to'=>$this->input->post('email'),'message'=>$message,'subject'=>'Verify your email address');
                if($this->functions->_email($email))
                {
                    $this->session->set_userdata('verify_email',$this->input->post('email'));
                    echo json_encode(['status'=>TRUE]);
                }
                else {
                    echo json_encode(['status'=>TRUE,'msg'=>'Email not sent.']);
                } */
            }else{
                echo json_encode(['status'=>FALSE,'msg'=>'SOmething went wrong. Try Again.']);
            } 
        }else{

            $pagedata = ['userd'=>$this->userd];
            $pagedata['currencies'] = $this->db->get_where('tbl_currency',['status'=>1])->result();
            $pagedata['languages'] = $this->db->get_where('tbl_language',['status'=>1])->result();
            $pagedata['countries'] = $this->db->get_where('tbl_country')->result();
            $pagedata['auth_type'] = $this->input->post('auth_type');
            $pagedata['social_id'] = $this->input->post('social_id');
            $this->load->view('pages/header_vw',$pagedata);
            $this->load->view('pages/host_register_vw');
        } 
    }
    
    function verify_email()
    {
        $email = $this->input->get('pram1');
        $code=  $this->input->get('pram2');
        $res = $this->db->select('id')->where(['md5(email)'=>$email,'verification_code'=>$code,'verified'=>0])->get('users');
        if($res->num_rows()>0)
        {
            $this->db->where(['md5(email)'=>$email,'verification_code'=>$code])->update('users',['verification_code'=>0,'verified'=>1]);
            $this->session->unset_userdata('verify_email');
            $this->session->set_userdata('user_id',$res->row()->id);
            $pagedata['content'] = 'Email Verified successfuly';
            $pagedata['status'] = 'success';
        }else{
            $pagedata['content'] = 'Invalid link or Link expired.';
            $pagedata['status'] = 'danger';
        }
        $this->load->view('common/header_vw',$pagedata);
        $this->load->view('pages/verify_email_vw');
        $this->load->view('common/footer_vw');
        
    }
    
    function resend_to_email()
    {
        $this->_is_logged();
        if($this->session->has_userdata('verify_email'))
        {
            $code=substr(md5(rand(000,9999)),5,6);
            $emailid = md5($this->session->verify_email);
            $message="Welcome,<br><br>and thank you for signing up to AgileVirgin! <br>Please verify your email by using <a href='".BASE."auth/verify-email?pram1=$emailid&pram2=$code'><b>this link</b></a><br><br>Regards<br>The AgileVirgin Team";
            $email=array('to'=>$this->session->verify_email,'message'=>$message,'subject'=>'Verify your email address');
            if($this->functions->_email($email))
            {
                $this->db->where(['email'=>$this->session->verify_email])->update('users',['verification_code'=>$code]);
                echo json_encode(['status'=>TRUE]);
            }else{
                echo json_encode(['status'=>FALSE,'error'=>'Something went wrong, Try again.']);
            }
        }else{ 
            echo json_encode(['status'=>FALSE,'error'=>'Invalid request']); 
        }
    }
    
    function forgot_password() 
    {
        $this->_is_logged();
        $this->form_validation->set_rules('email','Email','trim|required|xss_clean|valid_email');
        if($this->form_validation->run())
        {
            $forgotten = $this->db->select('id')->get_where('users',array('email'=>$this->input->post('email')));
            if($forgotten->num_rows()==1)
            {
                $code=substr(md5(rand(000,9999)),5,6);
                $emailid = md5($this->input->post('email'));
                $message="Please reset your password by using <a href='".BASE."auth/reset-password?pram1=$emailid&pram2=$code'><b>this link</b></a><br><br>Regards<br>The AgileVirgin Team";
                $email=array('to'=>$this->input->post('email'),'message'=>$message,'subject'=>'Reset password');
                if($this->functions->_email($email))
                {
                    $new_data=array('val'=>array('forgot_key'=>$code),'where'=>array('email'=>$this->input->post('email')),'table'=>'users');
                    $this->db->where(['email'=>$this->input->post('email')])->update('users',['forgot_key'=>$code]);
                    echo json_encode(['status'=>TRUE]);
                }else{
                    echo json_encode(['status'=>FALSE,'msg'=>'Email Not sent, Please try again.']);
                }
            }else
            {
                echo json_encode(['status'=>FALSE,'msg'=>'Email not registered with us.']);
            }
        }else{
            echo json_encode(['status'=>FALSE,'msg'=>'Enter Email ID.']);
        }
    }
    
    function message()
    {
        if(!$this->session->flashdata('msg'))
        {
            redirect(BASE);
            exit;
        }
        $pagedata['msg'] = $this->session->flashdata('msg');
        $this->load->view('common/header_vw',$pagedata);
        $this->load->view('pages/message_vw');
        $this->load->view('common/footer_vw');
    }
        
    function reset_password()
    {
        $this->_is_logged();
        $email = $this->input->get('pram1');
        $code=  $this->input->get('pram2');
        $res = $this->db->select('id,email')->where(['md5(email)'=>$email,'forgot_key'=>$code])->where('forgot_key <>',0)->get('users');
       if($res->num_rows()==1)
        {
           $this->session->set_userdata('reset',$res->row()->email);
            $pagedata['title'] = 'Reset Password';
            $this->load->view('common/header_vw',$pagedata);
            $this->load->view('pages/reset_password_vw');
            $this->load->view('common/footer_vw');
        }else{ 
            $this->session->set_flashdata('msg','Link expired or Invalid.');
            redirect('auth/message', 'refresh');
            
        }
    }
              
    function set_password()
    {
        if($this->session->reset)
        {
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[20]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required');
            if($this->form_validation->run())
            {
                $this->db->update('users',['password'=>md5(HASH.$this->input->post('password'))],['email'=>$this->session->reset]);
                $message="Hello User<br>Your Password has been successfully changed.";
                $email=array('to'=>$this->session->reset,'message'=>$message,'subject'=>'Password Changed Successfully');
                if($this->functions->_email($email))
                {
                    $this->session->unset_userdata('reset');
                    echo json_encode(['status'=>TRUE]);
                }
            }else{
                $error = ['password_err'=>  form_error('password','<div class="err">','</div>'),'confirm_password_err'=>  form_error('confirm_password','<div class="err">','</div>')];
                echo json_encode(['status'=>FALSE,'error'=>$error,'msg'=>'Error Occured']);
            }
        }else{
            echo json_encode(['status'=>FALSE,'msg'=>'Not Authorized.']);
        }
    }
    
    function submit_query()
    {
        $this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('subject','Subject','trim|required');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        $this->form_validation->set_rules('mobile','mobile','trim|required|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('query','Query','trim|required');
        if($this->form_validation->run())
        {
            $message="Hi Admin,<br><br>Received new query from <br><br>Name: ".$this->input->post('name')."<br><br>Email: ".$this->input->post('email')."<br><br>Mobile".$this->input->post('mobile')."<br><br>Message: ".$this->input->post('query')."<br><br>Regards<br>The AgileVirgin Team";
            $email=array('to'=>'support@agilevirgin.in','message'=>$message,'subject'=>  $this->input->post('subject'));
            $this->functions->_email($email);
            echo json_encode(['status'=>TRUE]);
        }else{
            $error = ['name_err'=>  form_error('name','<div class="err">','</div>'),'email_err'=>  form_error('email','<div class="err">','</div>'),'subject_err'=>  form_error('subject','<div class="err">','</div>'),
                        'mobile_err'=>  form_error('mobile','<div class="err">','</div>'),'query_err'=>  form_error('query','<div class="err">','</div>'),'name_err'=>  form_error('name','<div class="err">','</div>')];
            echo json_encode(['status'=>FALSE,'error'=>$error,'msg'=>'Error Occured']);
        }
    }
            
    function logout()
    {
        $this->session->sess_destroy();
        redirect(BASE);
    }
}