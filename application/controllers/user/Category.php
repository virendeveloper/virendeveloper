<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Controller {
    private $userd;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('menus');
        //$this->load->library('users');
        //$this->userd = $this->users->get_user($this->session->user_id,'id,name,email,mobile');
    }
    
    function index()
    {
        $this->load->view('users/common/header_vw');
        $this->load->view('users/category/category_vw');
        $this->load->view('users/common/footer_vw');
    }
    
    function add()
    {
        $data['parents'] = $this->db->get_where('tbl_category',['level'=>1,'status'=>1])->result();
        $this->load->view('users/common/header_vw',$data);
        $this->load->view('users/category/add_vw');
        $this->load->view('users/common/footer_vw');
    }
    
    function upload_image()
    {
        if($_FILES['image']['name']!='')
        {
            $file = $this->functions->_upload_image('image','assets/uploads/category','png|jpg|jpeg',3000);
            if($file['status'])
            {
                $_POST['image'] = $file['filename'];
                return TRUE;
            }else{
                $this->form_validation->set_message('upload_image',$file['error']);
                return FALSE;
            }
        }
    }
    
    function add_category()
    {
        $this->form_validation->set_rules('category_name','Category name','trim|required|xss_clean');
        $this->form_validation->set_rules('parent','Parent','trim|xss_clean');
        if($_FILES['image']['name']!='')
        $this->form_validation->set_rules('image','Image','trim|callback_upload_image');
        if($this->form_validation->run())
        {
            $parent = 0;
            $level=1;
            if($this->input->post('parent'))
            {
                $level=2;
                $parent = $this->input->post('parent');
            }
            $data = ['category_name'=>  $this->input->post('category_name'),'level'=>$level,'category_type'=>1,'parent_id'=>$parent,'status'=>1,'created_at'=>date('Y-m-d H:i:s')];
            if($_FILES['image']['name']!='')
            {
                $data['category_image'] = $this->input->post('image');
            }
            $this->db->insert('tbl_category',$data);
            echo json_encode(['status'=>TRUE]);
        }else{
            $error = ['category_name_err'=>  form_error('category_name','<div class="err">','</div>'),'image_err'=>  form_error('image','<div class="err">','</div>')];
            echo json_encode(['status'=>FALSE,'error'=>$error,'msg'=>'Error Occured']);
        }
    }
    
    function edit($id)
    {
        $data['category'] = $this->db->get_where('tbl_category',['_id'=>$id])->row();
        $data['parents'] = $this->db->get_where('tbl_category',['level'=>1,'status'=>1,'_id <>'=>$id])->result();
        $this->load->view('users/common/header_vw',$data);
        $this->load->view('users/category/add_vw');
        $this->load->view('users/common/footer_vw');
    }
    
    function edit_category()
    {
        $this->form_validation->set_rules('category_id','Category ID','trim|required|xss_clean');
        $this->form_validation->set_rules('category_name','Category name','trim|required|xss_clean');
        $this->form_validation->set_rules('parent','Parent','trim|xss_clean');
        if($_FILES['image']['name']!='')
        $this->form_validation->set_rules('image','Image','trim|callback_upload_image');
        if($this->form_validation->run())
        {
            $parent = 0;
            $level=1;
            if($this->input->post('parent'))
            {
                $level=2;
                $parent = $this->input->post('parent');
            }
            $data = ['category_name'=>  $this->input->post('category_name'),'level'=>$level,'parent_id'=>$parent];
            if($_FILES['image']['name']!='')
            {
                $data['category_image'] = $this->input->post('image');
            }
            $this->db->update('tbl_category',$data,['_id'=>  $this->input->post('category_id')]);
            echo json_encode(['status'=>TRUE]);
        }else{
            $error = ['category_name_err'=>  form_error('category_name','<div class="err">','</div>'),'image_err'=>  form_error('image','<div class="err">','</div>')];
            echo json_encode(['status'=>FALSE,'error'=>$error,'msg'=>'Error Occured']);
        }
    }
    
    function call_category($id)
    {
        $this->form_validation->set_rules('search','Search','trim|xss_clean');
        if($this->form_validation->run())
        {
            $count_res = $this->menus->get_category_count();
            
            $this->load->library("pagination");
            $config = array();
            $config["base_url"] = BASE . "user/category/call-category/";
            $config["total_rows"] = $count_res;
            $perpage=$config["per_page"] = $this->input->post('perpage');
            $config["uri_segment"] = 4;

            $this->pagination->initialize($config);
            $res = $this->menus->get_category_all($perpage,$id);
            //echo $this->db->last_query();die;
            $res['links'] = '';
            if($count_res > $perpage)
            {
                $res['links'] = $this->pagination->create_links();
            }
            $res['query'] = $count_res;
            echo json_encode($res);
        }
    }
}