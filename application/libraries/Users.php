<?php
class Users {
    var $table;
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->table = 'tbl_users';
    }
        
    function get_user($id,$fields="")
    {
        if($id)
        {
            if(isset($fields) && (is_array($fields) || trim($fields)!='')){
                $this->ci->db->select($fields);
            }

            $res=$this->ci->db->get_where($this->table,array('_id'=>$id));
            if($res->num_rows()>0)
            {
                return $res->row();
            }else{
                return false;
            }
        }
    }
   
     function _valid_user()
    {
        $refferer = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        if(!$this->ci->session->user_id)
        {
            redirect('auth/login','refresh');
        }
    }
    
    function _valid_admin()
    {
        $refferer = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        if(!$this->ci->session->admin_id)
        {
            redirect('panel/auth','refresh');
        }
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

