<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['about'] = 'auth/about';
$route['host-login-splash'] = 'auth/host_login_splash';
$route['onboarding'] = 'auth/onboarding';
$route['onboarding2'] = 'auth/onboarding2';
$route['onboarding3'] = 'auth/onboarding3';
$route['onboarding4'] = 'auth/onboarding4';
$route['onboarding5'] = 'auth/onboarding5';
$route['host-login'] = 'auth/host-login';
$route['host-login-social-networks'] = 'auth/host-login-social-networks';
$route['host-verification'] = 'auth/host-verification';
$route['host-login-verify-phoneno'] = 'auth/host-login-verify-phoneno';
$route['host-login-verify-email-address'] = 'auth/host-login-verify-email-address';
$route['register-now'] = 'auth/register-now';
$route['host-register'] = 'auth/host_register';
$route['myproposals'] = 'user/myproposals';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
